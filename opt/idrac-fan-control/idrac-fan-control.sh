#!/bin/bash
#--------------------------------------------------------------------------------------------------#
#-- Name     : idrac-fan-control.sh
#-- Purpose  : Fan controller for Dell iDrac based servers
#-- Author   : Vernetzen
#-- Repo     : https://gitlab.com/vernetzen/idrac-fan-control
#-- Requires : ipmitool, Dell iDrac enabled server
#-- Reference: https://www.spxlabs.com/blog/2019/3/16/silence-your-dell-poweredge-server
#-- License  : BSD https://opensource.org/licenses/BSD-3-Clause
#------------:
#-- Date     : 2020-06-08
VERSION="2.8.2"
#--------------------------------------------------------------------------------------------------#
# Generic vars
LANG=en_US

## Script name for systemd logging
SCRIPTNAME="idrac-fan-control"

## Prints errors
function _self.error() {
    echo -e "$1"
    exit $2;
}

## Prints banner info
function _banner() {
    cat<<EOF
-----------------------------------------------------------------------------------
Script  : $SCRIPTNAME
Purpose : Fan controller for Dell iDrac based servers
Author  : Vernetzen
Repo    : https://gitlab.com/vernetzen/idrac-fan-control
Version : $VERSION
-----------------------------------------------------------------------------------
EOF
}

## Base variables
DATE=`date +%Y%m%d-%H%M`
debug="no"
help="no"
quiet="no"
run="no"
skip="no"
config="none"
ORDINAL="5"

## Set defaults for GetOpts (standalone exec / not using config file)
idrac_hostname="local"
idrac_username=""
idrac_password=""
cpu_max_temp="60"
dynamic="no"
logging="file"
logfile="/var/log/idrac-fan-control.log"
pidfile="/var/run/${SCRIPTNAME}.pid"

## Obtain CPU sockets + status of CPU P-states + quantity + turbo state
function cpu_states() {
    export QUANT=`${IPMICMD} sdr type processor | grep -i status | wc -l`
}

## Obtain CPU info for local machine
function cpu_local_metrics() {
    export PS_SOCKETS=$(lscpu | grep -i "^socket" | awk -F: '{print $2}' | sed 's/ //g')
    export PS_CORES=$(cat /proc/cpuinfo | grep -i "^processor" | wc -l)
    export PS_STATUS=$(cat /sys/devices/system/cpu/intel_pstate/status)
    export PS_QUANT=$(cat /sys/devices/system/cpu/intel_pstate/num_pstates)
    TURBO=$(cat /sys/devices/system/cpu/intel_pstate/no_turbo)
    if [[ $TURBO -eq 0 ]]; then
	export PS_TURBO="enabled"
    elif [[ $TURBO -eq 1 ]]; then
	export PS_TURBO="disabled"
    fi
}

## Prints help info
function _self.help() {
    _banner
    cat<<EOF
Default Settings
-------------------
hostname:	${idrac_hostname}
username:	${idrac_username}
password:	${idrac_password}
cputemp:	${cpu_max_temp}
dynamic:        ${dynamic}
debug:          ${debug}
logging:	${logging}
logfile:	${logfile}
config:		${config}


Config Defaults
----------------
 -c, --config        Configuration file to use (overrides command line flags)
                     [default: none]

Manual Settings
----------------
 -h, --hostname      idrac hostname [default: none/local]
 -u, --username	     idrac username [default: root]
 -p, --password	     idrac password [default: calvin]
 -t, --cputemp       CPU temp threshold celsius [default: 60]
 -s, --sleep         Time in seconds between polling [default: 60]

Logging Options
----------------
 -l, --logging	     Set logging to file or systemd
                     [file/systemd] [default: file]

 -o, --logfile	     Full path of logfile for 'file' based logging
                     [default: /var/log/idrac-fan-control.log]

 -p, --pidfile       PID file, full path required
                     [default: /var/run/idrac-fan-control.pid]

Run Mode Flags
---------------
 -d, --dynamic       Flag to set fans to Dynamic Mode (ignore --cputemp) [default: not set]
 -q, --quiet         Run with non-verbose output [default: not set]
 -x, --debug         Debug output [default: not set]

Execute Program
----------------
--exec               Set flag to execute the program

Operational Note
----------------
Realtime CPU MHz stats are only available when using '--hostname local' as this metric
is not available via IPMI. Script relies on '/proc/cpuinfo' for MHz metric gathering.

Firmware Notes
----------------
Older versions of the iDrac and BIOS firmware list the fan RPM sensors as 'Fan2A RPM'.
This script was developed with BIOS v2.9.0 + iDrac firmware v2.65.65.65, which does
not list 'RPM' in the sensor string. If you are not getting the average fan speed info
in logging output then your firmware is likely out of date. Please ensure you're
running the versions previously listed or newer.

EOF

    if [[ ${idrac_hostname} = "local" ]]; then
	cpu_local_metrics
	echo -e "Local CPU Metrics -----------------------------------------------------------------"
	echo "CPU SOCKETS:  ${PS_SOCKETS}"
	echo "CPU CORES:    ${PS_CORES}"
	echo "CPU P-STATE:  ${PS_STATUS}"
	echo "CPU P-STATES: ${PS_QUANT}"
	echo "CPU TURBO:    ${PS_TURBO}"
	echo "-----------------------------------------------------------------------------------"
    fi
}

## Check ARGV array
if [ $# -eq 0 ]; then
    _self.help;
    exit 127;
else
    if [ $# -eq 1 ]; then
	if [ ${1} = "-h" ]; then
	    _self.help; exit 127;
	elif [ ${1} = "--help" ]; then
	    _self.help; exit 127;
	else
	    skip="no";
	fi
    fi
fi

## Check/Set GetOpts
if [ $skip = "no" ]; then
    while [ $# -gt 0 ]; do
	case $1 in
	    ## short opts
	    -h|--help) help="yes" ;;
	    -x|--debug) debug="yes" ;;
	    -q|--quiet) quiet="yes" ;;
	    -d|--dynamic) dynamic="yes" ;;
	    -e|--exec) run="yes" ;;
	    ## long opts need additional shift
	    -c|--config) config="$2" ; shift;;
	    -t|--cputemp) cpu_max_temp="$2" ; shift;;
	    -h|--hostname) idrac_hostname="$2" ; shift;;
	    -p|--password) idrac_password="$2" ; shift;;
	    -u|--username) idrac_username="$2" ; shift;;
	    -l|--logging) logging="$2" ; shift;;
	    -p|--pidfile) pidfile="$2" ; shift;;
	    -o|--logfile) logfile="$2" ; shift;;
	    -s|--sleep) sleep="$2" ; shift;;
	    (--) shift; break;;
	    (-*) echo "$0: error - unrecognized option $1" 1>&2; exit 1;;
	    (*) break;;
	esac
	shift
    done
fi

## Config file options take precidence over cli args
## Enabled via '--config' being set as a runtime variable. Parse config file and set values.
if [ "$config" != "none" ]; then
    function config_read_file() {
	(grep -E "^${2}=" -m 1 "${1}" 2>/dev/null || echo "VAR=__UNDEFINED__") | head -n 1 | cut -d '=' -f 2-;
    }

    function config_get() {
	val="$(config_read_file ${config} "${1}")";
	if [ "${val}" = "__UNDEFINED__" ]; then
	    echo "Failed to parse config file [$config] for required variable [${val}]"
	    exit 1;
	fi
	printf -- "%s" "${val}";
    }

    cpu_max_temp=$(config_get cputemp);
    debug=$(config_get debug);
    dynamic=$(config_get dynamic);
    idrac_hostname=$(config_get hostname);
    idrac_username=$(config_get username);
    idrac_username=$(config_get password);
    logfile=$(config_get logfile);
    logging=$(config_get logging);
    pidfile=$(config_get pidfile);
    quiet=$(config_get quiet);
    run=$(config_get exec);
    sleep=$(config_get sleep);
fi

## Execute check
if [ "${run}" != "yes" ]; then
    echo "Execution halted. Option arg '--exec' not set or config file var 'exec=yes' not set."
    exit 1;
fi

## Write the PID file
PIDNUM=${BASHPID}
PIDLOC=${pidfile}
echo "${PIDNUM}" > ${PIDLOC}

## Display config info
if [ "${quiet}" != "yes" ]; then
    if [ "${logging}" = "file" ]; then
	_banner
        cat<<EOF
Runtime Variables

hostname:	${idrac_hostname}
username:	${idrac_username}
password:	${idrac_password}
cputemp:	${cpu_max_temp}
sleep:          ${sleep}
dynamic:        ${dynamic}
debug:          ${debug}
quiet:		${quiet}
logging:	${logging}
logfile:	${logfile}
pidfile:	${pidfile}
config:		${config}
RPM +/-:        ${ORDINAL}

EOF
    fi
fi

## Logging Failures
function fail() {
    MSG=$1
    NOW=`date +%Y%m%d-%H%M%S`
    if [ "${logging}" = "systemd" ]; then
	which systemd-cat >/dev/null 2>&1
	if [ $? -ne 0 ]; then
	    echo "[FAILURE][$NOW] Could not locate systemd-cat binary in PATH. Use '--logging file' instead."
	    exit 1;
	else
	    echo "[ERROR] ${MSG}" | systemd-cat -t ${SCRIPTNAME}
	fi
    elif [ "$logging" = "file" ]; then
	echo "[ERROR][$NOW] ${MSG}" >> ${logfile}
    else
	echo "[FAILURE][$NOW] Option --logging specified incorrectly."
	_self.help
    fi
}

## Logging Status
function status() {
    MSG=$1
    NOW=`date +%Y%m%d-%H%M%S`
    if [ "${logging}" = "systemd" ]; then
	which systemd-cat >/dev/null 2>&1
	if [ $? -ne 0 ]; then
	    echo "[FAILURE][$NOW] Could not locate systemd-cat binary in PATH. Use '--logging file' instead."
	    exit 1;
	else
	    echo "${MSG}" | systemd-cat -t ${SCRIPTNAME}
	fi
    elif [ "${logging}" = "file" ]; then
	echo "[$NOW] ${MSG}" >> ${logfile}
    else
	echo "[FAILURE][$NOW] Option --logging specified incorrectly."
	_self.help
    fi
}

## Start
status "Starting idrac-fan-control version $VERSION"
status "Init Date: ${DATE}"
status "Ordinal: ${ORDINAL}"

## Enable/disable debug output
if [ "$debug" = "yes" ]; then
    set -x
else
    set +x
fi

## Determine if we use ipmitool with/without login or direct from local system
if [ "${idrac_hostname}" = "local" ]; then
    IPMICMD="ipmitool"
else
    IPMICMD="ipmitool -I lanplus -H ${idrac_hostname} -U ${idrac_username} -P ${idrac_password}"
fi

## CPU temperature sensor name
SENSORNAME="Temp"
CPUMAX="${cpu_max_temp}"

## Example raw IPMI code
## Configurable HEX value for speed = last set. Hex '0a' = 10%, sent as '0x0a'
# ${IPMICMD} raw 0x30 0x30 0x02 0xff 0x0a

## IPMI commands for setting dynamic/manual fan control mode
MODE_DYNAMIC="${IPMICMD} raw 0x30 0x30 0x01 0x01"
MODE_STATIC="${IPMICMD} raw 0x30 0x30 0x01 0x00"

## Outputs the average clock speed of all active CPU cores
function cpuspeed() {
    if [[ ${idrac_hostname} = "local" ]]; then
	export CPU_MHZ=$(cat /proc/cpuinfo | grep -i "cpu mhz" | awk -F: '{print $2}' | awk -F. '{print $1}' | awk '{print $1$2$3$4}' | tr '\n' ' ' | awk '{s+=$1}END{print s/NR}' RS=' ' | awk -F. '{print $1}')
    else
	export CPU_MHZ="0"
    fi
}

## Set fans to dynamic mode if requested, then exit
if [ "${dynamic}" = "yes" ]; then
    status "Setting fan mode to dynamic + exiting process."
    ${MODE_DYNAMIC}
    exit 0;
fi

## Poll sensors for fan RPMs and average results
function fanrpm() {
    # Polling method from OLD iDrac/BIOS versions where fans included 'RPM' in the sensor name
    #RPM=`${IPMICMD} -c sensor reading 'Fan2A RPM' 'Fan2B RPM' 'Fan3A RPM' 'Fan3B RPM' 'Fan4A RPM' 'Fan4B RPM' 'Fan5A RPM' 'Fan5B RPM' | tr '\n' ' ' | sed 's/RPM,//g' | awk '{print $2,$4,$6,$8,$10,$12,$14,$16}' | awk '{s+=$1}END{print "Fan Average RPM:",s/NR}' RS=' '`

    # Polling method for BIOS v2.9.0, iDrac firmware v2.65.65.65
    RPM=`${IPMICMD} -c sensor reading 'Fan2A' 'Fan2B' 'Fan3A' 'Fan3B' 'Fan4A' 'Fan4B' 'Fan5A' 'Fan5B' | tr '\n' ' ' | sed 's/ /,/g' | awk -F, '{print $2,$4,$6,$8,$10,$12,$14,$16}' | awk '{s+=$1}END{print s/NR}' RS=' '`
    export RPM="${RPM}"
}

## Proceed with static fan control if not using dynamic mode
## Retrieve current CPU temp
function get_temps() {
    # poll average cpu MHz
    cpuspeed
    # number of CPUs to check (R320 = 1, R420/R620 = 2)
    if [ ${QUANT} -eq 1 ]; then
	## get temp of CPU0 and CPU1
	T0=$(${IPMICMD} sdr type temperature | grep "^${SENSORNAME}" | cut -d"|" -f5 | cut -d" " -f2 | head -n 1)
	#AVG=$T0
	export AVG=${T0}

    elif [ ${QUANT} -eq 2 ]; then
	## get temp of CPU0 and CPU1
	T0=$(${IPMICMD} sdr type temperature | grep "^${SENSORNAME}" | cut -d"|" -f5 | cut -d" " -f2 | head -n 1)
	T1=$(${IPMICMD} sdr type temperature | grep "^${SENSORNAME}" | cut -d"|" -f5 | cut -d" " -f2 | tail -n 1)
	TEMPS=`expr ${T0} + ${T1}`
	TEMPAVG=`expr ${TEMPS} / 2`
	#AVG=$TEMPAVG
	export AVG=${TEMPAVG}

    else
	fail "script does not support more than 2 CPU sockets for temperature polling, exiting."
	exit 1;
    fi

}

## Delay reading values for N seconds
function pause() {
    sleep ${sleep} >/dev/null 2>&1
}

## Dynamic mode loop
function loopDYN() {
    INTEMP=$1
    ${MODE_DYNAMIC} >/dev/null 2>&1
    pause
    fanrpm
    get_temps
    status "Mode dynamic:  (CPU-AVG=${AVG}C @ ${CPU_MHZ}MHz, MAX=${CPUMAX}C, FAN=DYN%, RPM=${RPM}, EVAL-TEMP=${INTEMP}C)"

    # Stay in dynamic mode until temp reduces from max
    while [[ ${AVG} -ge ${CPUMAX} ]]; do
	pause
	fanrpm
	get_temps
	status "Mode dynamic:  (CPU-AVG=${AVG}C @ ${CPU_MHZ}MHz, MAX=${CPUMAX}C, FAN=DYN%, RPM=${RPM}, EVAL-TEMP=${INTEMP}C)"
    done

    # Jump out of dynamic mode if temp drops under the function input value, calculated to be a rounded int
    if [[ ${AVG} -lt ${CPUMAX}  ]]; then
	STEP=$(echo "scale=2; ${RPM} / 12000 / 10" \* 1000 | bc | awk -F. '{print $1}')
	loopVAR ${AVG} ${STEP};
    fi
}

## Variable RPM according to CPU average temp
function loopVAR() {
    INTEMP=$1
    INSPEED=$2
    if [[ $INSPEED -eq 5 ]]; then INSPEED="05"; fi
    cpuspeed
    status "Init start:    (CPU-AVG=${AVG}C @ ${CPU_MHZ}MHz, MAX=${CPUMAX}C, FAN=${INSPEED}%, RPM=${RPM}, EVAL-TEMP=${INTEMP}C)"
    ${MODE_STATIC} >/dev/null 2>&1

    ## Execute generated command, decimal to hex value for INSPEED RPM
    ${IPMICMD} raw 0x30 0x30 0x02 0xff 0x$(printf "%x\n" ${INSPEED}) >/dev/null 2>&1

    # Wait for '--sleep' duration
    pause

    # Poll + output average RPM
    fanrpm

    # Poll current CPU temp (result = AVG variable)
    get_temps
    status "Mode evaluate: (CPU-AVG=${AVG}C @ ${CPU_MHZ}MHz, MAX=${CPUMAX}C, FAN=${INSPEED}%, RPM=${RPM}, EVAL-TEMP=${INTEMP}C)"

    # Loop until temp changes sufficiently to require +/- RPM adjustment
    while [[ ${AVG} -le ${CPUMAX} ]]; do
	pause
	get_temps
	if [ "$quiet" != "yes" ]; then
	    status "Mode looping:  (CPU-AVG=${AVG}C @ ${CPU_MHZ}MHz, MAX=${CPUMAX}C, FAN=${INSPEED}%, RPM=${RPM}, EVAL-TEMP=${INTEMP}C)"
	fi

	# Reduce RPM by one ordinal value
	if [[ ${AVG} -lt ${INTEMP} ]]; then
	    NEWSPEED=$(expr ${INSPEED} - ${ORDINAL})

	    # Never go below 5% fan speed
	    if [[ ${NEWSPEED} -le 5 ]]; then
		loopVAR ${AVG} 5
	    else
		status "Mode switch:   (reducing to ${NEWSPEED}%)"
		loopVAR ${AVG} ${NEWSPEED}
	    fi
	fi
    done

    # Increase RPM by one ordinal value from current speed
    if [ ${AVG} -gt ${CPUMAX} ]; then
	status "Mode over-max: (CPU-AVG=${AVG}C @ ${CPU_MHZ}MHz, MAX=${CPUMAX}C, FAN=${INSPEED}%, RPM=${RPM}, EVAL-TEMP=${INTEMP}C)"
	NEWSPEED=$(expr ${INSPEED} + ${ORDINAL})

	if [[ ${NEWSPEED} -ge 100 ]]; then
	    status "Mode status: fans are at MAX speed, switching to dynamic mode"
	    loopDYN ${VAR}
	else
	    status "Mode switch:   (increasing to ${NEWSPEED}%)"
	    loopVAR ${AVG} ${NEWSPEED}
	fi
    fi
}

## Determine number of CPUs via IPMI for local or remote connections
cpu_states

## Display CPU metrics only if we're operating on the local host
if [[ ${idrac_hostname} = "local" ]]; then
    cpu_local_metrics
    status "CPU SOCKETS:  ${QUANT}"
    status "CPU CORES:    ${PS_CORES}"
    status "CPU P-STATE:  ${PS_STATUS}"
    status "CPU P-STATES: ${PS_QUANT}"
    status "CPU TURBO:    ${PS_TURBO}"
fi

## Start the loop
status "CPU MAX-TEMP: ${cpu_max_temp} Celsius"
while true; do
    fanrpm
    get_temps
    status "State check loop START"
    loopVAR ${AVG} 5
    get_temps
    status "State check loop END"
    pause
done

exit 0;
