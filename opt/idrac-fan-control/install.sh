#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cp -f /opt/idrac-fan-control/idrac-fan-control.conf.dist /etc/default/idrac-fan-control.conf
cp -f /opt/idrac-fan-control/idrac-fan-control.service /etc/systemd/system/idrac-fan-control.service
systemctl daemon-reload
