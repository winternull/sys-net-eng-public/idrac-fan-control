# Vernetzen: idrac-fan-control
System fan controller for iDrac based servers that alters fan speed up/down by an interval according to configured CPU temperature maximum value. Useful for homelabs or environments where the default fan algorithms make the system too loud despite the CPU temperature being lower than thermal junction maximum value.

## Base Requirements
- Bash 4.x
- General Linux with systemd if you want to run the script with the included service definition
- Other *NIX if you want to set up the script to run via non-systemd service framework
- Dell server with iDrac card. Tested on R320, R420, R730xd models

### iDrac Communication Modes
- A) ipmitool installed for LAN based connection to iDrac (requires user+pass) OR...
- B) ipmitool + OpenIPMI installed for local host based connection to iDrac

## Installation (Systemd Enabled)
1. Run installer via: `sudo bash ./opt/idrac-fan-control/install.sh`
2. Edit config file in `/etc/default/idrac-fan-control.conf`
3. Run the service: `sudo systemctl start idrac-fan-control`
4. Enable the service at boot (optional): `sudo systemctl enable idrac-fan-control`

## Manual Operating Mode
1. Copy the config into place: `sudo cp opt/idrac-fan-control/idrac-fan-control.conf.dist /etc/default/idrac-fan-control.conf`
2. Edit the config file and change defaults if desired
3. Run the script: `bash ./opt/idrac-fan-control/idrac-fan-control.sh --config /etc/default/idrac-fan-control.conf`

## Status Info
You can monitor the status of fan speed and CPU temp via either syslog or the custom log file, as specified in the config file.

## Usage Notes
- Check your specific CPU model's thermal junction temperature maximum value before changing default temperature in the config. Some CPUs have a higher max operating temp, eg 75C or greater, but many will require 68C or lower to remain in stable range.
- Keep in mind that this service does not take into account system case exit temperature, which can be important when running a system with all PCIe slots full of cards that require high air pressure to keep cool.
- All systems are not created equal. Know your hardware and understand the risks that come from modifying system defaults before using.
- Script can be configured to run in 'dynamic' fan mode if desired, which hands off fan control to the iDrac (factory mode)

## Command Flag Defaults
```
» ./idrac-fan-control.sh -h
-----------------------------------------------------------------------------------
Script  : idrac-fan-control
Purpose : Fan controller for Dell iDrac based servers
Author  : Vernetzen
Repo    : https://gitlab.com/vernetzen/idrac-fan-control
Version : 2.8.2
-----------------------------------------------------------------------------------
Default Settings
-------------------
hostname:       local
username:
password:
cputemp:        60
dynamic:        no
debug:          no
logging:        file
logfile:        /var/log/idrac-fan-control.log
config:         none


Config Defaults
----------------
 -c, --config        Configuration file to use (overrides command line flags)
                     [default: none]

Manual Settings
----------------
 -h, --hostname      idrac hostname [default: none/local]
 -u, --username      idrac username [default: root]
 -p, --password      idrac password [default: calvin]
 -t, --cputemp       CPU temp threshold celsius [default: 60]
 -s, --sleep         Time in seconds between polling [default: 60]

Logging Options
----------------
 -l, --logging       Set logging to file or systemd
                     [file/systemd] [default: file]

 -o, --logfile       Full path of logfile for 'file' based logging
                     [default: /var/log/idrac-fan-control.log]

 -p, --pidfile       PID file, full path required
                     [default: /var/run/idrac-fan-control.pid]

Run Mode Flags
---------------
 -d, --dynamic       Flag to set fans to Dynamic Mode (ignore --cputemp) [default: not set]
 -q, --quiet         Run with non-verbose output [default: not set]
 -x, --debug         Debug output [default: not set]

Execute Program
----------------
--exec               Set flag to execute the program
```
